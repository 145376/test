const Mock = require("mockjs");

const swiperData = Mock.mock({
  "swipers|3": [
    {
      "id|+1": 1,
      url: "@image('728x90', 'pink', '轮播图', 'png')",
    },
  ],
});

module.exports = {
  swiperData,
};
